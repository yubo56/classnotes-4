    \documentclass[10pt]{report}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage, hyperref, enumerate, graphicx, setspace, wasysym, upgreek, listings}
    \usepackage[margin=0.5in, top=0.8in,bottom=0.8in]{geometry}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<#1\,\middle|\,#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Im}{Im}
    \DeclareMathOperator{\Log}{Log}
    \DeclareMathOperator{\Arg}{Arg}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\E}{E}
    \DeclareMathOperator{\Var}{Var}
    \DeclareMathOperator*{\argmin}{argmin}
    \DeclareMathOperator*{\argmax}{argmax}
    \DeclareMathOperator{\sgn}{sgn}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    \usepackage[font=scriptsize]{subcaption}
    \everymath{\displaystyle}
    \lstset{basicstyle=\ttfamily\footnotesize,frame=single,numbers=left}

\tikzstyle{circ} = [draw, circle, fill=white, node distance=3cm, minimum height=2em]

\begin{document}

\title{Ph129c --- Probability and Statistics in Physics}
\author{Frank Porter\\
TTh 0900-1030}
\date{ }
\maketitle

\pagestyle{fancy}
\cfoot{\thepage/\pageref{LastPage}}

\chapter{03/31/16 --- Fundamentals of Statistics}

\section{Statistical Independence}

Call statistical independence the property $f_{XY}(x,y) = f_X(x)f_Y(y)$ (we use capital letters to denote RVs and lowercase to denote instances thereof). An important example is when one can sample independently multiple times from the same distribution, i.e. $f_X(x) = f_Y(y)$, then we call each sampling independent. More completely, we say that sampling multiple times from the same distribution is statistically independent iff
\begin{align}
    f_{\left\{ X \right\}}\left( \left\{ x \right\} \right) = \product_{k=1}^n f_{X_k}(x_k)
\end{align}
where $f{X_k} = f(X_j) \forall j, k$. These are independent, identically distributed variables (iid).

We call the expectation value of a function $u$ of a random variable (RV) $X$ as $\expvalue{u(X)} = \int\limits_{X}^{}u(x)f(x)\;\mathrm{d}x$. This generalizes to discrete random variables if we interpret the integral as a Lebesgue integral with discrete measure. Alterantive notations include $\expvalue{u(X)} = \E\left[ u(X) \right] = \overline{u(X)}$. Note that the expectation defines a linear function, i.e. $\expvalue{au(X) + bv(X)} = a\expvalue{u(X)} + b\expvalue{v(X)}$.

\begin{center}
    \textbf{Theorem:} $\expvalue{u(X)v(Y)} = \expvalue{u(X)}\expvalue{v(Y)}$ if $X,Y$ are statistically independent RVs.
\end{center}

This falls out of the definition of the expectation value, since
\begin{align}
    \expvalue{u(X)v(Y)} &= \int\limits_{X,Y}^{}u(x)v(y)f_{XY}(x,y)\;\mathrm{d}x\,\mathrm{d}y\\
    &= \int\limits_{X,Y}^{}u(x)v(y)f_X(x)f_Y(y)\;\mathrm{d}x\,\mathrm{d}y\\
    &= \int\limits_{X}^{}u(x)f_X(x)\;\mathrm{d}x \int\limits_{Y}^{}v(y)f_Y(y)\;\mathrm{d}y\\
    &= \expvalue{u(X)}\expvalue{v(Y)}
\end{align}

Important parameters of a distribution of a RV include
\begin{itemize}
    \item \emph{Mean} of RV is its expectation value.
    \item \emph{Variance} of RV is the square of its standard deviation (rms) and is given by the mean-squared deviation
        \begin{align}
            \Var(X) = \sigma_X^2 &= \expvalue{(X - \expvalue{X})^2}\\
            &= \expvalue{X^2} - \expvalue{X}^2
        \end{align}
        Observe that if $X,Y$ are statistically independent $\expvalue{XY} = \expvalue{X}\expvalue{Y}$ then
        \begin{align}
            \Var(X + Y) &= \expvalue{(X + Y)^2} - \expvalue{X + Y}^2 = \expvalue{X^2} + 2\expvalue{X}\expvalue{Y} + \expvalue{Y^2} + (\expvalue{X} + \expvalue{Y})^2\\
            &= \expvalue{X^2} - \expvalue{X}^2 + \expvalue{Y^2} - \expvalue{Y}^2 = \Var(X) + \Var(Y)
        \end{align}
        which also then relates the standard deviations $\sigma_{X+Y} = \sqrt{\sigma_X^2 + \sigma_Y^2}$.

    \item Variance generalizes in multivariate case to \emph{covariance matrix}. Consider random variables $\left\{ X_i \right\}$, then define
        \begin{align}
            M_{ij} &= \expvalue{(X_i - \expvalue{X_i})(X_j - \expvalue{X_j})} = \expvalue{X_iX_j} - \expvalue{X_i}\expvalue{X_j}
        \end{align}
        the covariance matrix. Observe that $M_{ii} = \Var(X_i)$ and the off-diagonal terms are the covariances.

        Define ``covariance coefficients'' by $\rho_{ij} = \frac{M_{ij}}{\sqrt{M_{ii}M_{jj}}}$, which describes the degree of linear correlation between $X_i, X_j$. Careful about assuming $\rho_{ij} = 0$ implies statistical independence, e.g. $X_i, X_j$ distributed on a circle would be obviously correlated that would have vanishing $\rho_{ij}$.
\end{itemize}

\section{Convergence}

Consider $\left\{ X_i \right\}$ a sequence of random variables, distributed according to cdf's $F_i$ respectively. If $X$ is a random variable with distribution $F$ and $\lim_{n \to \infty}F_n(x) = F(x)$ then we say that $\left\{ X_i \right\}$ \emph{converges in distribution} (or \emph{converges weakly}) to $X$. 

\subsection{Weak Convergence Example: Poisson Distribution}

For instance, consider distribution
\begin{align}
    P(k; n,p) &= \binom{n}{k}p^k\left( 1-p \right)^{n-k}
\end{align}

Let's investigate the limit as $N \to \infty$ with $pn = \theta$ held fixed. Well, it turns out that we can math pretty well
\begin{align}
    \lim_{n \to \infty} F_n(k) &= \lim_{n \to \infty}\sum\limits_{j \leq k}^{}\binom{n}{j}\left( \frac{\theta}{n} \right)^{j}\left( 1 - \frac{\theta}{n} \right)^{n-j}\\
    &= \sum\limits_{j \leq k}^{} \frac{\theta^j}{j!} \lim_{n \to \infty} \underbrace{\frac{n(n-1)\dots (n-j+1)}{n^j}}_{=1} \frac{\left( 1 - \frac{\theta}{n} \right)^n}{\left( 1 - \frac{\theta}{n} \right)^j}\\
    &= \sum\limits_{j \leq k}^{}\frac{\theta^je^{-\theta}}{j!}
\end{align}
which we recognize to be the cdf of the Poisson distribution $f(k) = \frac{\theta^ke^{-\theta}}{k!}$. This has properties (I cite results but we can show these obviously)
\begin{itemize}
    \item Mean $\expvalue{k} = \theta$.
    \item Variance $\expvalue{k^2} - \expvalue{k}^2 = \theta$.
\end{itemize}

\subsection{Stronger Notion of Convergence}

Let $\epsilon > 0$ and let $P(\abs{X_n - X} \geq \epsilon)$ be the probability that $X_n$ differs from $X$ by $\epsilon$. Then if given any $\epsilon > 0$ we have
\begin{align}
    \lim_{n \to \infty} P\left( \abs{X_n - X} \geq 0 \right) = 0
\end{align}
then we say that $X_i$ \emph{converges in probability} to $X$.

\section{Characteristic Functions}

We define the characteristic function of a pdf $f_X(x)$ to be
\begin{align}
    \phi_X(k) &= \int\limits_{-\infty}^{\infty}e^{ikx}f_X(x)\;\mathrm{d}x = \expvalue{e^{ikx}}
\end{align}
or more strongly notated $\phi_X(k) = \int\limits_{-\infty}^{\infty}e^{ikx}\;\mathrm{d}F_X(x)$ to capture the cases where $f$ is a discrete distribution.

This will come in handy when we discuss central limit theorem, but as one example the characteristic function for a binomial distribution is (some steps omtted)
\begin{align}
    \sum\limits_{n=0}^{N}e^{ikn}\binom{N}{n}p^n(1-p)^{N-n} = \left( 1-p + pe^{ik} \right)^n
\end{align}

\chapter{04/07/16 --- Simulation}

Consider $M_x, M_y$ covariance matricies $M_x = \expvalue{(X - \bar{x}) (x - \bar{x})^T}$. Then consider some afficen transformation $y = Tx + a$, then
\begin{align}
    M_y &= \expvalue{(Y - \bar{Y})(Y - \bar{Y})^T} \nonumber\\
    &= T\expvalue{(X - \bar{X})(X - \bar{X})^T}T^T = TM_XT^T
\end{align}

For an affine transformation, this is exact. If non-linear, we make the linear approximation $T_{ij} = \pd{Y_i}{X_j}\Bigg|_{X = \expvalue{X}}$. As an example, consider if $Y = Y(X_i)$, a one-dimensional variable, then 
\begin{align}
    M_Y = \sigma_Y^2 = \sum\limits_{i=1}^{N}\sum\limits_{j=1}^{N}\pd{Y}{x_i}\Bigg|_{X = \expvalue{X}}\pd{Y}{x_j}\Bigg|_{X = \expvalue{X}}
\end{align}

If all the $X_i$ are statistically independent, then $M_x$ is diagonal, and we obtain $\sigma_Y^2 = \sum\limits_{j=1}^{n}\left[ \pd{Y}{x_i}\Bigg|_{X = \expvalue{X}}\sigma_{X_i} \right]^2$

\section{Monte Carlo}

SUppose we want to evaluate $I = \int\limits_{0}^{1}\dots \int\limits_{0}^{1}f(x)\;\mathrm{d}^kx$. One way we can estimate numerically is to divide the hypercube of integration into $N^k$ regions and to evaluate $f$ at one point in each region. Then we can approximate
\begin{align}
    I_n = \frac{1}{N^k}\sum\limits_{\dots}^{N^k}f(x_{\nu})
\end{align}

If $f$ is ``reasonable'' (we have some intuition for this already), then this procedure converges to the value of the real integral as $N \to \infty$. This sounds about reasonable.

Now, Monte Carlo takes a different approach: randomly select $M$ points and average over these points. We draw from a uniform distribution of vectors within the box, and average by dividing by $M$. We expect that if $r_\nu$ are the randomly chosen points that
\begin{align}
    I = \lim_{M \to \infty} \frac{1}{M}\sum\limits_{\nu=1}^{M}f(r_\nu)
\end{align}
so long as again $f$ is ``nice.'' One cool bit of intuition is that the integral acts almost as an expectation value here. Let's make this rigorous by saying
\begin{align}
    I &= V_R I'' & I'' &= \int\limits_{R}^{}f(x)p(x)\;\mathrm{d}^kx = \expvalue{f}
\end{align}
with $V_R = \int_R \mathrm{d}^kx, \int p(x)\mathrm{d}^kx = 1$. Then consider $I_M' = \frac{1}{M}\sum\limits_{i=1}^{M}f(x_i)$ the Monte Carlo approximation with $M$ points. Does it converge to $\expvalue{f}$? Is it an unbiased estimator? Let's check
\begin{align}
    \expvalue{I_M'} &= \expvalue{\frac{1}{M}\sum\limits_{i=1}^{M}f(X_i)}\\
    &= \frac{1}{M}\sum\limits_{i=1}^{M}\expvalue{f(X_i)}\\
    &= \frac{1}{M}\sum\limits_{i=1}^{M}\int\limits_{R}^{}f(x)p(x)\mathrm{d}^kx\\
    &= \expvalue{f}
\end{align}

We can also show that our estimator is \emph{consistent} by showing that $\lim_{M \to \infty}I_M' = I''$. What about the variance?
\begin{align}
    \Var(I_M') &= \expvalue{(I_M' - \expvalue{I_m'})^2}\\
    &= \expvalue{(I_M')^2} + \expvalue{f}^2 - 2\expvalue{I_m'}\expvalue{f}\\
    &= \expvalue{(I_M')^2} - \expvalue{f}^2\\
    \expvalue{(I_M')^2} &= \expvalue{\frac{1}{M^2}\sum\limits_{i,j}^{}f(x_i)f(f_j)}\\
    &= \frac{1}{M^2}\left[ \sum\limits_{i}^{}\expvalue{[f(x_i)]^2} + \underbrace{\sum\limits_{i \neq j}^{}\expvalue{f(x_i)f(x_j)} \right]}_{\frac{M(M-1)}{M^2}\expvalue{f}^2}\\
    \Var(I_M') &= \frac{1}{M}\left[ \expvalue{f^2} - \expvalue{f}^2 \right] = \frac{1}{M}\Var(f)
\end{align}

\section{Sampling from PDF}

Sampling a specified pdf is usually at best pseudorandom, uniformly distributed on some range $\left[ 1,N \right]$, which is then divided by $N$ to get a ``real'' on (0,1). Sampling a PDF isn't always trivial, so let's consider the following example.

Consider if we have data conforming to some background except for an excess somewhere. Can we determine whether the bump is statistically significant? The background histogram is sampled from the distribution $f(n;b)$ with $n$ the total number of counts and $b$ the bin count
\begin{align}
    f(n;b) &= \sum\limits_{i=1}^{m}\frac{b_i^{n_i}e^{-b_i}}{n_i!}
\end{align}
with $m$ the number of bins, $b_i$ the expected number of counts in $b$ and $n_i$ the observed counts. This is just a product of Poisson distributions.

We can recast this as multiple MC experiments, sampling from this pdf, and examine each experiment to see whether there is a signal at least as strong as that observed.

\subsection{Inverse Transform Sampling}

In order to do this, we need a Poisson random number generator, starting with a uniform continuous distribution $U(0,1)$. We consider the cdf $F(x)$, then sample $u \in [0,1]$ uniformly, and then find $x, F(x) = u$, then $x$ will be distributed randomly according to $f(x)$, since $x = F^{-1}(u)$ in some sense. In more detail, we seek transformation $T$ such that $u \overset{T}{\longrightarrow} x, x$ is drawn from $f(x)$.

In order to effect this transformation, we can consider $f(X)\mathrm{d}x = f[T(u)]\abs{\rd{x}{u}}\mathrm{d}u = p(u)\mathrm{d}u$, or alternatively
\begin{align}
    f\left[ T(u) \right]\abs{\rd{T(u)}{u}} &= p(u)\\
    f[T(u)]\abs{\mathrm{d}T(u)} &= p(u)\mathrm{d}u
\end{align}

But then if we let $x \in [a,b]$, then we can choose a mapping such that $u=0 \to x=a$, then $\rd{T}{u} \geq 0$ and we can integrate the above and obtain
\begin{align}
    F(x) &= \int\limits_{a}^{x}f(x)\;\mathrm{d}x
\end{align}

Let's consider an example, suppose we want random numbers from $N(0,1)$ (mean $0$, stdev $1$) Then we get that
\begin{align}
    u &= \int\limits_{-\infty}^{x}\frac{1}{\sqrt{2\pi}}e^{-\frac{\xi^2}{2}}\;\mathrm{d}\xi
\end{align}

We see the difficulty of the inverse transform method; it's not always trivial to compute this expression! But we can do it here, by going to a two dimensional Gaussian 
\begin{align}
    f(x,y) \mathrm{d}x\mathrm{d}y &= \frac{1}{2\pi}e^{-x^2/2}e^{-y^2/2}\mathrm{d}x\mathrm{d}y\\
    &= \frac{\mathrm{d}\phi}{2\pi}re^{-r^2/2}\mathrm{d}r \equiv g(r)\mathrm{d}r\; h(\phi)\mathrm{d}\phi
\end{align}
then if we can randomly generate two random $u \in U(0,1)$ according to $g(r), h(\phi)$ using two uniform distributions, then we can effect
\begin{align}
    1-u &= \int\limits_{0}^{r}g(r')\;\mathrm{d}r'\\
    u &= \int\limits_{r}^{\infty}\rho e^{-\rho^2/2}\;\mathrm{d}\rho = e^{-r^2/2}\\
    r &= \sqrt{-2\ln u}\\
    v &= \int\limits_{0}^{\phi}h(\varphi)\;\mathrm{d}\varphi = \frac{\phi}{2\pi}\\
    \phi &= 2\pi v
\end{align}
then given $u,v$, we can generate \emph{two} Gaussian variables
\begin{align}
    x &= \sqrt{-2\ln u}\cos(2\pi v)\\
    y &= \sqrt{-2\ln u}\sin(2\pi v)
\end{align}
both of which are sampled from $N(0,1)$.

For a discrete distribution, we can use the same method, where we simply have to find the $u \in U(0,1)$ to generate $n$ such that
\begin{align}
    \sum\limits_{k=0}^{n-1}p(k) < u \leq \sum\limits_{k=0}^{n}p(k)
\end{align}

\section{No longer coming to class}

EOM
\end{document}

