    \documentclass[10pt]{article}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage, hyperref, enumerate, graphicx, setspace, wasysym, upgreek, listings}
    \usepackage[margin=0.5in, top=0.8in,bottom=0.8in]{geometry}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<#1\,\middle|\,#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Im}{Im}
    \DeclareMathOperator{\Log}{Log}
    \DeclareMathOperator{\Arg}{Arg}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\E}{E}
    \DeclareMathOperator{\Var}{Var}
    \DeclareMathOperator*{\argmin}{argmin}
    \DeclareMathOperator*{\argmax}{argmax}
    \DeclareMathOperator{\sgn}{sgn}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    \usepackage[font=scriptsize]{subcaption}
    \everymath{\displaystyle}
    \lstset{basicstyle=\ttfamily\footnotesize,frame=single,numbers=left}

\tikzstyle{circ} = [draw, circle, fill=white, node distance=3cm, minimum height=2em]

\begin{document}

\pagestyle{fancy}
\rhead{Yubo Su --- Independent Study: Ph161}
\cfoot{\thepage/\pageref{LastPage}}

\tableofcontents
\clearpage

\section{Day 1 --- Lorenz Model}

The Lorenz Model arises as an approximation to atmospheric flow, starting with the description of the simplest mode of a convection zone and dropping terms that represent higher harmonics. This gives the following system of equations
\begin{align}
    \dot{X} &= -\sigma(X - Y) \nonumber\\
    \dot{Y} &= rX - Y - XZ \nonumber\\
    \dot{Z} &= b(XY - Z)
\end{align}
with parameters canonically set at $\sigma = 10, b = 8/3, r =27 > 1$ (note that $\sigma$ is 0.7 for an ideal gas, 1--4 for water and $>10$ for oils). Note that the equations are dissipative, i.e. volumes in phase space shrink in dynamics. Note also that they are \emph{autonomous} in that there is no explicit time dependence on the right hand side, a convenient property.

It turns out that the solutions to this system are chaotic, or in Lorenz's original terms ``aperiodic.'' We do many demonstrations that are no longer accessible, including \emph{strange attractors, Poincar\'e section} and \emph{1D maps}.

The last is going to be covered in a later lecture, but in case I'm interested in simulating the former two, I look them up here (the website has discussions of these with the applets absent):
\begin{itemize}
    \item Poincar\'e section examines the intersection of an orbit with a particular plane in phase space, e.g. $Z=31$ is a common one. So we just simulate and examine every time that $Z=31$ is crossed, exactly what $(X,Y)$ are.
    \item Strange attractors are subsets of phase space with fractional dimension (fractals) that orbits at long times lie in.
\end{itemize}

\section{Day 2 --- Pendula}

\subsection{Ideal Pendulum}

The ideal pendulum is given by EOM $\rtd{\theta}{t} + \frac{g\sin\theta}{l} = 0$. We can plot phase space $(\theta, \omega = \dot{\theta})$ for solution trajectories. There are a few features of interest
\begin{itemize}
    \item Fixed points --- Two types, elliptic (stable but not asymptotically stable) and hyperbolic (unstable)
    \item Limit Cycles --- Solution trajectories.
    \item Homoclinic orbits --- Trajectories that connect the same hyperbolic fixed point. Heteroclinic orbits connect two different hyperbolic fixed points.
\end{itemize}

We can write down the Hamiltonian for this system $H = \frac{J^2}{2I} + Mgl(1-\cos\theta)$. The importance of being able to do is that phase space volume is preserved. What do we mean by this? Well, consider Hamilton's canonical equations
\begin{align}
    \dot{\theta} = \pd{H}{J} &= \frac{J}{I} \nonumber\\
    \dot{J} = -\pd{H}{\theta} &= -Mgl\sin\theta
\end{align}
and if we define some phase space velocity $\vec{V} = \left( \dot{\theta}, \dot{J} \right)$ we can verify that $\nabla \cdot \vec{V} = 0$. In other words, no attractors in phase space can exist for a Hamiltonian system.

\subsection{Dissipative Pendulum}

New EOM is $\rtd{\theta}{t} + \eta \rd{\theta}{t} + \frac{g}{l}\sin\theta = 0$. This creates a fixed point at $(0,0)$, asymptotically stable (``linearly stable'' is his terminology), and $(\pi,0)$ is suddenly linearly unstable, in that we will exponentially deviate from the point (before, we still periodically returned).

\subsection{Driving + damping}

Let's drive the pendulum and rescale variables to $\rtd{\theta}{t} + \gamma \rd{\theta}{t} + \sin\theta = g\cos(\omega_D t)$. Under small angle approximation (small amplitude of solution and driving), we know how to solve this. What about with larger driving amplitudes though?

To handle this, we wish to solve numerically, which is much easier with an autonomous system than with an explicit time dependence. Thus, we introduce $\dot{\theta}_D = \omega_D$ and suddenly we can write
\begin{align}
    \dot{\theta} &= \omega \nonumber\\
    \dot{\omega} &= -\gamma \omega - \sin\theta + g\cos(\theta_D) \nonumber\\
    \dot{\theta}_D &= \omega_D
\end{align}

This is a handy trick to get an autonomous system at the expense of an additional equation in the system of ODEs. We might also suspect that the behavior of the system changes when we get an extra dimension of phase space.

Well, let's start by observing that $\vec{\nabla} \cdot \vec{v} = -\gamma$ so phase space volumes contract. This might naively lead us to conclude that any volume of initial conditions must contract to a point, but we mustn't be hasty; the Lorenz model also has contracting phase space volumes. Instead, we know that we might approach a limit cycle in a way such that while the overall phase space contracts, the space might ``stretch'' along some dimensions and contract in others such that chaotic dynamics are observed! The demonstrations illustrating this are broken, but we will definitely simulate this.

\section{Day 3 --- Nonlinear Oscillators}

\subsection{Van der Pol Oscillator}

Consider the following model of a harmonic oscillator to include a non-linear damping. Damping is negative at small amplitudes to model local instability, but is positive for larger amplitudes; we thus expect the system to have some limit cycles where the average damping vanishes. The EOM is
\begin{align}
    \ddot{x} - \gamma(1-x^2)\dot{x} + x &= g\cos(\omega_D t)
\end{align}

\subsubsection{Small $\gamma$, secular perturbation}

Let's first look at oscillations with no driving, $g=0$. We return to the second order EOM for this. The natural starting point is to also examine for small $\gamma$ and try Lindstedt-Poincar\'e perturbation theory (also called secular perturbation theory) on
\begin{align}
    \ddot{x} + x &= \gamma(1-x^2)\dot{x}
\end{align}

We begin with the $\gamma=0$ solution, which is just $x(t) = ae^{it + \phi}$. Then, by secular perturbation theory \footnote{We learned as \emph{Lindstedt-Poincar\'e} in 106, but this seems to differ slightly; instead of setting up this slowly varying timescale as below, we tend to set $s = \omega t$ and expand $\omega = 1 + \epsilon \omega_1 +\dots$.}, when $\gamma \neq 0$, we can substitute $x(t) = A(t)\cos(t) + \gamma x_1 +\dots$ and also expect $A(t)$ to be some slow-varying function such that $\frac{\mathrm{d}^nA(t)}{\mathrm{d}t^n} \sim \gamma^n$ (this is introducing a second slow time scale, characteristic of secular perturbation theory apparently) and collect $O(\gamma^1)$ terms in the ODE to obtain
\begin{align}
    \rtd{}{t}\left[ A\cos(t) + \gamma x_1 \right] + A\cos(t) + \gamma x_1 &= \gamma(1 - A^2\cos^2(t))\rd{}{t}(A\cos(t)) \nonumber\\
    - 2\sin(t)\rd{A}{t} + \gamma\rtd{x_1}{t} + \gamma x_1 &= -\gamma A\sin(t) + \gamma A^3\underbrace{\cos^2(t)\sin(t)}_{\frac{\sin(t)}{4} + \frac{\sin(3t)}{4}} \nonumber\\
    \gamma\left(\rtd{x_1}{t} + x_1\right) &= -\gamma A\sin(t) + \gamma A^3\frac{\sin(t)}{4} + 2\sin(t)\rd{A}{t} + O(\sin(3t))
\end{align}
where we don't care about the $\sin(3t)$ term since it doesn't drive $x_1$ on resonance. On the other hand, we require the remaining term vanish, which implies that
\begin{align}
    2\rd{A}{t} = \gamma A - \frac{\gamma A^3}{4} = \gamma \frac{A(4 - A^2)}{4}
\end{align}
off of which we can read that any initial $A > 0$ tends to grow until $A = 2$, asymptoting at $x(t) = 2\cos(t)$.

\subsubsection{Large $\gamma$}

For large $\gamma$, let's stick to driving $g=0$ and go to a slightly unconventional choice of phase space variables, namely
\begin{align}
    \dot{y} &= \ddot{x} - \gamma(1-x^2)\dot{x} = -x\\
    \dot{x} &= y + \gamma\left( x - \frac{x^3}{3} \right)
\end{align}

We then rescle $y \to \gamma Y, t \to \gamma T, x \to X$ and introduce $\eta = \gamma^{-2}$ and obtain
\begin{align}
    \eta \rd{X}{T} &= Y + \left( X - \frac{X^3}{3} \right)\\
    \rd{Y}{T} &= -X
\end{align}
and for large $\gamma$ we now have small $\eta$. We first find that $\eta = 0$ is inconsistent; the first equation reduces to $Y = -\left( X - \frac{X^3}{3} \right)$, predicting a minimum of $Y = -\frac{2}{3}$ while the latter suggests $Y \to 0$ strictly decreasing-ly, contradictory. However, we can still analyze qualitatively what happens for small $\eta$, an orbit that comprises two pieces
\begin{itemize}
    \item $Y + \left( X - \frac{X^3}{3} \right)$ is small, so $Y \simeq \frac{X^3}{3} - X$ and $Y, X$ change on timescales $O(1)$.
    \item $Y + \left( X - \frac{X^3}{3} \right) \gg \eta\rd{X}{T}$, then $X$ changes very rapidly over timescale $O(\eta^{-1})$, and since $\rd{Y}{T} \sim O(1)$ we can treat $Y$ to be constant.
\end{itemize}

This breaks down to the orbit following $Y = \frac{X^3}{3} - X$ until reaching an extremum, at which point $\rd{X}{T} = 0$ and $Y + \left( X - \frac{X^3}{3} \right) \gg \eta \rd{X}{T}$, and suddenly we evolve at constant $Y$ until we hit another point on the $Y = \frac{X^3}{3} - X$ curve. The picture is then of following a cubic on $X < -1, X > 1$ and jumping from $\left(-1, \frac{2}{3}\right) \to \left( 2, \frac{2}{3} \right)$ and from $\left( 1, -\frac{2}{3} \right)\to \left( -2, -\frac{2}{3} \right)$ (the constant $Y$ portions of the curve).

This analysis allows us to compute $\dot{x} = v = \gamma\left[ Y - \left( -X + \frac{X^3}{3} \right) \right]$ as the difference between $\frac{X^3}{3}$ the cubic and $Y$ which tracks the cubic for $X < -1, X > 1$ and is constant during the jumps. We can simulate via Mathematica, and verify the shape of the trajectory.

This shows that there are free oscillations at any given $\gamma$.

\subsection{Driven Oscillations: Frequency Locking}

In general, when one drives an oscillator, one can either observe oscillations at both the free and driving frequency (in which case the power spectrum would show signatures at both frequencies), or we would see only a single frequency (and maybe harmonics). The latter case is called \emph{frequency locking}, when the driving frequency locks the free oscillation to some fixed frequency. The former case, when the driving and free oscillations are in an irrational ratio, is considered \emph{quasiperiodic}, because the sum of the two frequencies never repeats.

Another way to define frequency locking is that while some parameters of the problem (e.g. drive amplitude, dissipation) are varied, all peaks in the power spectrum must remain fixed for some non-zero parameter space. If the frequency is not locked, at least some peaks should vary continuously as parameters are varied continuously. It turns out that frequency locking occurs for the vDP oscillator for high drive amplitude and small frequency differences, which is generally true.

The basic idea behind analyzing frequency locking (beyond the scope of this class) is to take equation of motion
\begin{align}
    \ddot{x} + x &= \gamma(1-x^2)\dot{x} + g\cos(\omega_D t)
\end{align}
and substitute $\omega_D = 1 + \gamma \Delta$ and $g = 2\gamma F$ again in the $\gamma \ll 1$ limit, so weak driving near resonance. Performing the same secular perturbation theory (I concede, it is easier to use $x(t) = A(t)e^{it} + c.c. + \epsilon x_1$ here than cosines) and write driving term $\epsilon F e^{i \Delta \epsilon t}e^{it}$, we obtain that killing the secular term requires
\begin{align}
    \rd{A}{t} &= \frac{\gamma}{2}\left( A - \abs{A}^2 \right) - \gamma\frac{i}{2}Fe^{i\Delta \epsilon t}
\end{align}
then we can make the substitution $\tilde{A} = Ae^{-i\Delta \epsilon t}$ which yields
\begin{align}
    \rd{\tilde{A}}{t} + i\gamma \Delta \tilde{A} &= \frac{\gamma}{2}\left( \tilde{A}(1-\abs{\tilde{A}}^2) - iF \right)
\end{align}
upon which whether locking happens boils down to the nature of algebraic solutions to $\tilde{A}$ above. A stable \emph{fixed point} yields a locked solution (harmonics nearby tend to disappear) while stable \emph{limit cycles} produce unlocked solutions (two frequencies), since the Poincar\'e-Bendixson theorem tells us that no other asymptotic long term dynamics are possible. Unstable solutions are uninteresting; if both stable fixed and limit cycles exist then both locked and unlocked dynamics can manifest. The remainder of this is omitted.

It turns out that the vDP oscillator does not exhibit chaos.

\subsection{Duffing Oscillator}

The Duffing Oscillator, given by potential
\begin{align}
    V(x) &= \pm \frac{1}{2}x^2 + \frac{1}{4}x^4 + \frac{1}{4}
\end{align}
and EOM
\begin{align}
    \ddot{x} + \gamma \dot{x} \pm x + x^3 &= g\cos(\omega_D t)
\end{align}
does exhibit chaos for both signs apparently!

\section{Day 4 --- One Dimensional Maps}

Let's consider an arbitrary system $\dot{U} = f(U|r)$ with $r$ some control parameters and $U$ a vector of phase space coordinates, some autonomous system.

\end{document}

